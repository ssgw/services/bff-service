﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Mvc;
using SWSG.BffService.CoreAccess;
using SWSG.BffService.Domain;

namespace SWSG.BffService.Api.Controllers;

[ApiController]
[Route("api/v{version:apiVersion}/property-energy-data")]
[ApiVersion("1.0")]
[Consumes("application/json")]
public sealed class PropertyEnergyDataController : ControllerBase
{
    private IPropertyDataService PropertyDataService { get; }

    public PropertyEnergyDataController(IPropertyDataService propertyDataService)
    {
        PropertyDataService = propertyDataService;
    }

    [HttpGet]
    public async Task<ActionResult<PropertyEnergyData>> GetPropertyEnergyDataAsync([FromQuery] string street, [FromQuery] string zipCode, [FromQuery]string city)
    {
        PropertyEnergyData? propertyEnergyData = await PropertyDataService.GetPropertyEnergyDataAsync(street, zipCode, city);

        return HasPropertyEnergyData(propertyEnergyData)
            ? Ok(propertyEnergyData)
            : NotFound();
    }

    private static bool HasPropertyEnergyData([NotNullWhen(true)] PropertyEnergyData? propertyEnergyData)
        => propertyEnergyData is not null;
}