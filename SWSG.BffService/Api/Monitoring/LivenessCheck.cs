﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using SWSG.BffService.CoreAccess;

namespace SWSG.BffService.Api.Monitoring;

public sealed class LivenessCheck: IIsAliveCheck
{
    private IMonitoringService MonitoringService { get; }

    public LivenessCheck(IMonitoringService monitoringService)
    {
        MonitoringService = monitoringService;
    }

    public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = new())
    {
        bool hasBeenSuccessful = await MonitoringService.IsServiceHealthyAsync();
        return await Task.FromResult(hasBeenSuccessful ? HealthCheckResult.Healthy() : HealthCheckResult.Unhealthy());
    }
}