﻿namespace SWSG.BffService.Api.Monitoring;

public record HealthCheckMappingOptions
{
    public string? CorsPolicy { get; init; }
    public string[]? AuthorizationPolicies { get; init; }
    public string[]? Hosts { get; init; }
    internal string Path { get; init; }
    internal string Name { get; init; }
    internal bool HasCorsPolicy => !string.IsNullOrWhiteSpace(CorsPolicy);
    internal bool HasAuthorization => HasValidItems(AuthorizationPolicies);
    internal bool HasHosts => HasValidItems(Hosts);

    private bool HasValidItems(string[]? strings)
        => !(strings == null || strings.Length == 0 || strings.All(string.IsNullOrWhiteSpace));
}