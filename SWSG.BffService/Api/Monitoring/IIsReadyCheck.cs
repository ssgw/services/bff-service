﻿using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace SWSG.BffService.Api.Monitoring;

public interface IIsReadyCheck : IHealthCheck
{
}