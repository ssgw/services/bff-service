﻿using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace SWSG.BffService.Api.Monitoring.Extensions
{
    public static class HealthCheckExtension
    {
        private const string IS_ALIVE = "Liveness";
        private const string IS_ALIVE_PATH = "/healthz/live";
        private const string IS_READY = "Readiness";
        private const string IS_READY_PATH = "/healthz/ready"; 

        public static IServiceCollection AddHealthChecks<TIsAliveHealthCheck, TIsReadyHealthCheck>(
            this IServiceCollection services)
            where TIsAliveHealthCheck : class, IIsAliveCheck, IHealthCheck
            where TIsReadyHealthCheck : class, IIsReadyCheck, IHealthCheck
        {
            services.AddHealthChecks()
                .AddCheck<TIsAliveHealthCheck>(IS_ALIVE, HealthStatus.Unhealthy, new[] { IS_ALIVE })
                .AddCheck<TIsReadyHealthCheck>(IS_READY, HealthStatus.Unhealthy, new[] { IS_READY });
            return services;
        }

        public static void MapHealthChecks(this IEndpointRouteBuilder endpoints,
            HealthCheckMappingOptions? options = null)
        {
            HealthCheckMappingOptions mappingOptions = options ?? new();
            endpoints.MapHealthCheck(mappingOptions with { Path = IS_ALIVE_PATH, Name = IS_ALIVE });
            endpoints.MapHealthCheck(mappingOptions with { Path = IS_READY_PATH, Name = IS_READY });
        }

        private static void MapHealthCheck(this IEndpointRouteBuilder endpoints, HealthCheckMappingOptions options)
        {
            IEndpointConventionBuilder endpointBuilder = endpoints.MapHealthChecks(options.Path, CreateOptions(options.Name));
            endpointBuilder.AllowAnonymous();

            if (options.HasHosts)
                endpointBuilder.RequireHost(options.Hosts!);
        }

        private static HealthCheckOptions CreateOptions(string filterTag)
            => new()
            {
                AllowCachingResponses = false,
                Predicate = check => check.Name.Contains(filterTag),
                ResultStatusCodes =
                {
                    [HealthStatus.Healthy] = StatusCodes.Status200OK,
                    [HealthStatus.Degraded] = StatusCodes.Status200OK,
                    [HealthStatus.Unhealthy] = StatusCodes.Status503ServiceUnavailable
                }
            };
    }
}