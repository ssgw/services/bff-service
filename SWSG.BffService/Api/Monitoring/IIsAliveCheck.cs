﻿using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace SWSG.BffService.Api.Monitoring;

public interface IIsAliveCheck : IHealthCheck
{
}