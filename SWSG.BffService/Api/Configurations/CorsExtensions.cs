﻿namespace SWSG.BffService.Api.Configurations;

internal static class CorsExtensions
{
    private const string CORS_POLICY_NAME = "CorsPolicy";

    internal static IServiceCollection AddAllowAllCors(this IServiceCollection services)
        => services.AddCors(options =>

            options.AddPolicy(CORS_POLICY_NAME, builder =>
            {
                builder
                    .AllowAnyOrigin()
                    .AllowAnyMethod()
                    .AllowAnyHeader();
            })
        );

    internal static IApplicationBuilder UseAllowAllCors(this IApplicationBuilder app)
        => app.UseCors(CORS_POLICY_NAME);
}