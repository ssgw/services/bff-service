﻿using Microsoft.EntityFrameworkCore;
using SWSG.BffService.Constants;
using SWSG.BffService.Data;
using SWSG.BffService.Domain.Settings;

namespace SWSG.BffService.Api.Configurations;

internal static class ServiceConfiguration
{
    internal static void ConfigureService(this IServiceCollection serviceCollection, IConfiguration configuration)
        => serviceCollection
            .ConfigureDatabase(configuration)
            .ConfigureEgidWebServiceSettings(configuration);

    private static IServiceCollection ConfigureDatabase(this IServiceCollection serviceCollection,
        IConfiguration configuration)
    {
        string? dbConnectionString = configuration.GetConnectionString(Db.EnergyDataManagement.CONNECTION_STRING_NAME);
        serviceCollection.AddDbContext<ServiceDbContext>(options => options.UseNpgsql(dbConnectionString,
            x => x.MigrationsHistoryTable("_MigrationHistory", Db.EnergyDataManagement.SCHEMA)));
        return serviceCollection;
    }

    private static IServiceCollection ConfigureEgidWebServiceSettings(this IServiceCollection serviceCollection,  IConfiguration configuration)
        => serviceCollection.AddSingleton(_ => configuration.GetSection("EgidWebServiceSettings").Get<EgidWebServiceSettings>());
}