﻿using SWSG.BffService.CoreAccess;

namespace SWSG.BffService.Api.Configurations;

internal static class ServiceDataStructureConfiguration
{
    internal static async Task ApplyDataStructureAsync(this IApplicationBuilder builder)
    {
        using IServiceScope scope = builder.ApplicationServices.CreateScope();
        IServiceProvider serviceProvider = scope.ServiceProvider;
        IPersistenceService service = serviceProvider.GetRequiredService<IPersistenceService>();
        await service.SetUpDataStructureAsync();
    }
}