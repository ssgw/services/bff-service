﻿using System.Diagnostics.CodeAnalysis;
using System.Reflection;
using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.Swagger;
using Swashbuckle.AspNetCore.SwaggerUI;

namespace SWSG.BffService.Api.Configurations;

internal static class SwaggerExtensions
{
    internal static IServiceCollection AddSwagger(this IServiceCollection services, string apiName)
        => services
            .AddSwaggerGen(config =>
                {
                    config.SwaggerDoc("v1", new OpenApiInfo
                    {
                        Title = apiName,
                        Version = "v1",
                    });

                    config.EnableAnnotations(enableAnnotationsForInheritance: true,
                        enableAnnotationsForPolymorphism: true);
                    config.UseAllOfForInheritance();
                    config.UseOneOfForPolymorphism();

                    string xmlFileName = CreateAssemblyXmlFileName();
                    string? xmlFilePath = GetXmlFilePathOrDefault(xmlFileName);

                    if (HasXmlPath(xmlFilePath))
                        config.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilePath));
                }
            )
            .AddSwaggerGenNewtonsoftSupport();

    private static string CreateAssemblyXmlFileName()
        => $"{Assembly.GetEntryAssembly()?.GetName().Name}.xml";
    
    private static string? GetXmlFilePathOrDefault(string xmlFileName) =>
        Directory
            .GetFiles(Directory.GetCurrentDirectory(), xmlFileName, SearchOption.AllDirectories)
            .FirstOrDefault();

    private static bool HasXmlPath([NotNullWhen(true)] string? xmlFilePath)
        => !string.IsNullOrWhiteSpace(xmlFilePath);

    internal static IApplicationBuilder UseSwagger(this IApplicationBuilder app, string baseUrl)
    {
        if (string.IsNullOrWhiteSpace(baseUrl))
            app.UseSwagger();
        else
            app.UseSwagger(c => ApplyBehindReverseProxyOptions(c, baseUrl));
        
        return app
            .UseSwaggerUI(options =>
            {
                options.DefaultModelRendering(ModelRendering.Model);
                options.DefaultModelExpandDepth(2);
            });
    }
    
    private static void ApplyBehindReverseProxyOptions(SwaggerOptions c, string baseUrl)
    {
        c.PreSerializeFilters.Add((swaggerDoc, httpRequest) =>
        {
            if (HasNoReversProxyInfos(httpRequest)) 
                return;
            
            string serverUrl = CreateServerUrl(baseUrl, httpRequest);
            swaggerDoc.Servers = new List<OpenApiServer> { new() { Url = serverUrl } };
        });
    }

    private static string CreateServerUrl(string basePath, HttpRequest httpRequest) 
        => $"{httpRequest.Scheme}://{httpRequest.Headers["X-Forwarded-Host"]}{basePath}";

    private static bool HasNoReversProxyInfos(HttpRequest httpRequest)
        => !httpRequest.Headers.ContainsKey("X-Forwarded-Host");
}