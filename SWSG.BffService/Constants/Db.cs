﻿namespace SWSG.BffService.Constants;

public static class Db
{
    public static class EnergyDataManagement
    {
        public const string CONNECTION_STRING_NAME = "DbString";
        public const string SCHEMA = "ernergy-data";

        public static class Table
        {
            public const string BUILDINGS = "buildings";
            public const string REMOTE_HEATING = "remote_heatings";
        }
    }
}