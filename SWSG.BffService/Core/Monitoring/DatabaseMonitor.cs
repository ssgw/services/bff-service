﻿using SWSG.BffService.DataAccess;

namespace SWSG.BffService.Core.Monitoring;

internal sealed class DatabaseMonitor : IDatabaseMonitor
{
    private IDatabaseProbeRepository DatabaseProbeRepository { get; }
    private ILogger<DatabaseMonitor> Logger { get; }
    private IMigrationRepository MigrationRepository { get; }

    public DatabaseMonitor(IDatabaseProbeRepository databaseProbeRepository, ILogger<DatabaseMonitor> logger,
        IMigrationRepository migrationRepository)
    {
        DatabaseProbeRepository = databaseProbeRepository;
        Logger = logger;
        MigrationRepository = migrationRepository;
    }

    async Task<bool> IMonitor.IsHealthyAsync()
        => await IsDatabaseAliveCheckAsync(CheckType.Liveness);

    private async Task<bool> IsDatabaseAliveCheckAsync(CheckType checkType)
    {
        try
        {
            return await DatabaseProbeRepository.IsDatabaseAliveAsync();
        }
        catch (Exception ex)
        {
            LogNoConnectionToDatabase(checkType, ex);
            return false;
        }
    }

    private void LogNoConnectionToDatabase(CheckType checkType, Exception ex)
    {
        if (Logger.IsEnabled(LogLevel.Warning))
            Logger.LogWarning($"Service is not {GetCheckType(checkType)} - cannot connect to database", ex);
    }

    private static string GetCheckType(CheckType checkType)
        => checkType == CheckType.Liveness ? "healthy" : "ready";

    async Task<bool> IMonitor.IsReadyAsync()
        => await IsDatabaseAliveCheckAsync(CheckType.Readiness) && await HasNoPendingMigrationAsync();

    private async Task<bool> HasNoPendingMigrationAsync()
    {
        if (await MigrationRepository.HasNoPendingMigrationAsync())
            return true;

        LogPendingMigration();
        return false;
    }

    private void LogPendingMigration()
    {
        if (Logger.IsEnabled(LogLevel.Information))
            Logger.Log(LogLevel.Information, "Service is not ready - has pending database migrations");
    }
}