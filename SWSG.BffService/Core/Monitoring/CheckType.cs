﻿namespace SWSG.BffService.Core.Monitoring;

public enum CheckType
{
    Liveness,
    Readiness
}