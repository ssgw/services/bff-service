﻿namespace SWSG.BffService.Core.Monitoring;

public interface IMonitor
{
    internal Task<bool> IsHealthyAsync();
    internal Task<bool> IsReadyAsync();
}
