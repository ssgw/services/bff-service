﻿using System.Diagnostics.CodeAnalysis;
using SWSG.BffService.CoreAccess;
using SWSG.BffService.DataAccess;
using SWSG.BffService.Domain;

namespace SWSG.BffService.Core;

public sealed class PropertyDataService : IPropertyDataService
{
    private IEgidWebService EgidWebService { get; }
    private IPropertyEnergyDataRepository PropertyEnergyDataRepository { get; }

    public PropertyDataService(IEgidWebService egidWebService,
        IPropertyEnergyDataRepository propertyEnergyDataRepository)
    {
        EgidWebService = egidWebService;
        PropertyEnergyDataRepository = propertyEnergyDataRepository;
    }

    public async Task<PropertyEnergyData?> GetPropertyEnergyDataAsync(string street, string zipCode, string city)
    {
        string? egid = await GetEgidByAddressAsync(street, zipCode, city);

        if (HasNoEgid(egid))
            return null;

        return await PropertyEnergyDataRepository.GetByEgidAsync(egid);
    }

    private async Task<string?> GetEgidByAddressAsync(string street, string zipCode, string city)
    {
        string formattedAddress = CreateAddress(street, zipCode, city);
        return await EgidWebService.GetEgidByAddressAsync(formattedAddress);
    }

    private static string CreateAddress(string street, string zipCode, string city)
        => $"{street} {zipCode} {city}";

    private static bool HasNoEgid([NotNullWhen(false)] string? egid)
        => egid is null;
}