﻿using SWSG.BffService.CoreAccess;
using SWSG.BffService.DataAccess;

namespace SWSG.BffService.Core;

public sealed class PersistenceService : IPersistenceService
{
    private IMigrationRepository MigrationRepository { get; }

    public PersistenceService(IMigrationRepository migrationRepository)
    {
        MigrationRepository = migrationRepository;
    }

    public async Task SetUpDataStructureAsync()
        => await MigrationRepository.MigrateAsync();
}
