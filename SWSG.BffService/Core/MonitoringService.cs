﻿using SWSG.BffService.Core.Monitoring;
using SWSG.BffService.CoreAccess;

namespace SWSG.BffService.Core;

public sealed class MonitoringService: IMonitoringService
{
    private IReadOnlyList<IMonitor> Monitors { get; }

    public MonitoringService(IDatabaseMonitor databaseMonitor)
    {
        Monitors = new List<IMonitor>
        {
            databaseMonitor
        }.AsReadOnly();
    }

    public async Task<bool> IsServiceHealthyAsync()
        => (await Task.WhenAll(Monitors.Select(monitor => monitor.IsHealthyAsync()))).All(isHealthy => isHealthy);

    public async Task<bool> IsServiceReadyAsync()
        => (await Task.WhenAll(Monitors.Select(monitor => monitor.IsReadyAsync()))).All(isReady => isReady);
}