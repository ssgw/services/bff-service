﻿namespace SWSG.BffService.CoreAccess;

public interface IPersistenceService
{
    Task SetUpDataStructureAsync();
}