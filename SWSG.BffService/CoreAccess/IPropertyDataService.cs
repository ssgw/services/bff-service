﻿using SWSG.BffService.Domain;

namespace SWSG.BffService.CoreAccess;

public interface IPropertyDataService
{
    Task<PropertyEnergyData?> GetPropertyEnergyDataAsync(string street, string zipCode, string city);
}