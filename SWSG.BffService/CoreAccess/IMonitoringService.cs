﻿namespace SWSG.BffService.CoreAccess;

public interface IMonitoringService
{
    Task<bool> IsServiceHealthyAsync();
    Task<bool> IsServiceReadyAsync();
}