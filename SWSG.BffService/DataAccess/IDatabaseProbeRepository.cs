﻿namespace SWSG.BffService.DataAccess;

public interface IDatabaseProbeRepository
{
    public Task<bool> IsDatabaseAliveAsync();
}