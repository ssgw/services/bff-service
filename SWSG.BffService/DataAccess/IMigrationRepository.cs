﻿namespace SWSG.BffService.DataAccess;

public interface IMigrationRepository
{
    public Task<bool> HasNoPendingMigrationAsync();
    public Task MigrateAsync();
}