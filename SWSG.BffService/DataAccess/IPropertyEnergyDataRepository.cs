﻿using SWSG.BffService.Domain;

namespace SWSG.BffService.DataAccess;

public interface IPropertyEnergyDataRepository
{
    public Task<PropertyEnergyData?> GetByEgidAsync(string egid);
}