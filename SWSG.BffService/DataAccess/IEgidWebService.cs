﻿namespace SWSG.BffService.DataAccess;

public interface IEgidWebService
{
    public Task<string?> GetEgidByAddressAsync(string address);
}