﻿namespace SWSG.BffService.Domain;

public enum HeatSupplyStrategy
{
    Anergy,
    AnergyPlanned,
    Wood,
    Heatpump,
    LocalHeating
}