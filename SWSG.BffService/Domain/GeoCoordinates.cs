﻿namespace SWSG.BffService.Domain;

public record GeoCoordinates
{
    public double Latitude { get; init; }
    public double Longitude { get; init; }
}