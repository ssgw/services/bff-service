﻿namespace SWSG.BffService.Domain.Settings;

public record EgidWebServiceSettings
{
    public string BaseUrl { get; init; } = string.Empty;
}