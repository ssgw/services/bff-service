﻿namespace SWSG.BffService.Domain;

public enum BuildingDistrictHeatingConnection
{
    Connected,
    Planned,
    NotPlanned
}