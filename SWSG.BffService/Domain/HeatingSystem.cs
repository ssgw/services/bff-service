﻿namespace SWSG.BffService.Domain;

public record HeatingSystem
{
    public int HeatProducer { get; init; }
    public int EnergyHeatSource { get; init; }
    public int InformationSource { get; init; }
    public string ActualizationDate { get; init; } = string.Empty;
    public BuildingDistrictHeatingConnection DistrictHeatingConnection { get; init; }
    public HeatingSource HeatingSource { get; init; }
    public HeatSupplyStrategy HeatSupplyStrategy { get; init; }

    public bool OptimalHeatSupplyAchieved { get; set; }
        
    public bool IsDistrictHeatingRecommended { get; set; }

    public bool IsHeatpumpRecommended { get; set; }

    public bool IsWoodRecommended { get; set; }
}