﻿namespace SWSG.BffService.Domain;

public record PropertyEnergyData()
{
    public long Id { get; init; }
    public string Egid { get; init; } = string.Empty;
    public string BuildingClassification { get; init; }
    public int ConstructionYear { get; init; }
    public int BuildingSurface { get; init; }
    public string BuildingVolumes { get; init; } = string.Empty;
    public string EnergyReferenceSurface { get; init; } = string.Empty;
    public Address Address { get; init; } = new();
    public Boiler Boiler { get; init; } = new();
    public HeatingSystem HeatingSystem { get; init; } = new();
    public Kitchen Kitchen { get; init; } = new();
}