﻿namespace SWSG.BffService.Domain;

public record Boiler
{
    public int HeatProducer { get; init; }
    public int EnergyHeatSource { get; init; }
    public int InformationSource { get; init; }
    public string ActualizationDate { get; init; } = string.Empty;
}