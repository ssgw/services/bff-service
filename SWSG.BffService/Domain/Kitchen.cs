﻿namespace SWSG.BffService.Domain;

public record Kitchen
{
    public string CookingDevice { get; init; }
}