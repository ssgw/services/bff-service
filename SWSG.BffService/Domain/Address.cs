﻿namespace SWSG.BffService.Domain;

public record Address
{
    public string Street { get; init; } = string.Empty;
    public int ZipCode { get; init; }
    public string City { get; init; } = string.Empty;
    public string Municipality { get; init; } = string.Empty;
    public string Canton { get; init; } = string.Empty;
    public GeoCoordinates GeoCoordinates { get; init; } = new();
}