﻿using SWSG.BffService.Data.Contracts;
using SWSG.BffService.Domain;

namespace SWSG.BffService.Data.Mapping;

internal static class DataEnergyPropertyMapper
{
    internal static PropertyEnergyData ToPropertyEnergyData(this BuildingEntity entity)
    {
        var data = new PropertyEnergyData
        {
            Id = entity.Id,
            Egid = entity.Egid ?? string.Empty,
            BuildingVolumes = entity.Gvol ?? string.Empty,
            BuildingClassification = (entity.Gklas ?? -1).ToBuildingClassText(),
            BuildingSurface = entity.Garea ?? -1,
            ConstructionYear = entity.Gbauj ?? -1,
            EnergyReferenceSurface = entity.Gebf ?? string.Empty,
            Address = new Address
            {
                Street = entity.StrnameDeinr ?? string.Empty,
                ZipCode = entity.Dplz4 ?? -1,
                City = string.Empty, //entity.Gbez ?? string.Empty,
                Municipality = entity.Ggdename ?? string.Empty,
                Canton = entity.Gdekt ?? string.Empty,
                GeoCoordinates = new GeoCoordinates
                {
                    Longitude = entity.Longitude ?? 0,
                    Latitude = entity.Latitude ?? 0
                },
            },
            Boiler = new()
            {
                HeatProducer = entity.Gwaerzw1 ?? -1,
                EnergyHeatSource = entity.Genw1 ?? -1,
                InformationSource = entity.Gwaerscew1 ?? -1,
                ActualizationDate = entity.Gwaerdatw1 ?? string.Empty,
            },
            HeatingSystem = new()
            {
                HeatProducer = entity.Gwaerzh1 ?? -1,
                EnergyHeatSource = entity.Genh1 ?? -1,
                InformationSource = entity.Gwaersceh1 ?? -1,
                ActualizationDate = entity.Gwaerdath1 ?? string.Empty,
                DistrictHeatingConnection = entity.HestiaBeschreibung switch
                {
                    "Fernw\u00e4rme in Betrieb" => BuildingDistrictHeatingConnection.Connected,
                    "kein Fernw\u00e4rmegebiet" => BuildingDistrictHeatingConnection.NotPlanned,
                    { Length: > 0 } description when description.StartsWith("Phase") =>
                        BuildingDistrictHeatingConnection.Planned,
                    _ => BuildingDistrictHeatingConnection.NotPlanned
                },
                HeatingSource = entity.Genh1.HasValue ? (HeatingSource)entity.Genh1 : HeatingSource.None,
                HeatSupplyStrategy = entity.WaermeVersorgung switch
                {
                    "Anergie in Betrieb" => HeatSupplyStrategy.Anergy,
                    { Length: > 0 } heatSupply when heatSupply.Contains("Anergie Ausbaugebiet")
                                                    || heatSupply.Contains("Anergie Erg\u00e4nzungsgebiet") =>
                        HeatSupplyStrategy.AnergyPlanned,
                    "dezentral erneuerbar, ausserhalb Bauzone" => HeatSupplyStrategy.Wood,
                    "dezentral W\u00e4rmepumpen" => HeatSupplyStrategy.Heatpump,
                    "W\u00e4rmeverbunde fördern" => HeatSupplyStrategy.LocalHeating,
                    _ => HeatSupplyStrategy.Anergy
                },
            },
            Kitchen = new()
            {
                CookingDevice = entity.Wkche ?? string.Empty,
            }
        };

        data.HeatingSystem.OptimalHeatSupplyAchieved =
            data.HeatingSystem.HeatingSource == HeatingSource.DistrictHeatingGeneric
            || (data.HeatingSystem.HeatSupplyStrategy == HeatSupplyStrategy.Heatpump
                && new[] { HeatingSource.Geothermal, HeatingSource.GeothermalProbe }
                    .Contains(data.HeatingSystem.HeatingSource))
            || (data.HeatingSystem.HeatSupplyStrategy == HeatSupplyStrategy.Heatpump
                && new[] { HeatingSource.Geothermal, HeatingSource.GeothermalProbe }
                    .Contains(data.HeatingSystem.HeatingSource));

        data.HeatingSystem.IsDistrictHeatingRecommended =
            data.HeatingSystem.DistrictHeatingConnection != BuildingDistrictHeatingConnection.NotPlanned
            && data.HeatingSystem.HeatingSource != HeatingSource.DistrictHeatingGeneric;

        data.HeatingSystem.IsHeatpumpRecommended =
            data.HeatingSystem.HeatSupplyStrategy == HeatSupplyStrategy.Heatpump
            && !new[] { HeatingSource.Geothermal, HeatingSource.GeothermalProbe }.Contains(data.HeatingSystem
                .HeatingSource);

        data.HeatingSystem.IsWoodRecommended =
            data.HeatingSystem.HeatSupplyStrategy == HeatSupplyStrategy.Wood
            && !new[]
            {
                HeatingSource.WoodChips,
                HeatingSource.WoodPieces,
                HeatingSource.WoodGeneric,
                HeatingSource.WoodPieces
            }.Contains(data.HeatingSystem.HeatingSource);

        return data;
    }
}