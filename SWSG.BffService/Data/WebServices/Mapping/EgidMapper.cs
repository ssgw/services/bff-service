﻿using SWSG.BffService.Data.WebServices.Contracts;

namespace SWSG.BffService.Data.WebServices.Mapping;

internal static class EgidMapper
{
    internal static string ToEgid(this EgidAttributesDto attributs)
        => attributs.FeatureId[..^2];
}