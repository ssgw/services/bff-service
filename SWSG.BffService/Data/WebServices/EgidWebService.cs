﻿using System.Collections.Immutable;
using Flurl;
using Flurl.Http;
using SWSG.BffService.Data.WebServices.Contracts;
using SWSG.BffService.Data.WebServices.Mapping;
using SWSG.BffService.DataAccess;
using SWSG.BffService.Domain.Settings;

namespace SWSG.BffService.Data.WebServices;

public sealed class EgidWebService : IEgidWebService
{
    private string BaseUrl { get; }

    public EgidWebService(EgidWebServiceSettings webServiceSettings)
    {
        BaseUrl = webServiceSettings.BaseUrl;
    }

    public async Task<string?> GetEgidByAddressAsync(string address)
        => (
                await GetEgidAsync(address, "locations", "address", 1, 2056)
            )
            .FirstOrDefault()?
            .Attributes
            .ToEgid();

    private async Task<IImmutableList<EgidDto>> GetEgidAsync(string searchText, string type, string origins, int limit,
        int sr)
        => (await BaseUrl
                .SetQueryParam("searchText", searchText)
                .SetQueryParam("type", type)
                .SetQueryParam("origins", origins)
                .SetQueryParam("limit", limit)
                .SetQueryParam("sr", sr)
                .GetJsonAsync<EgidResultDto>()
            )
            .Egids
            .ToImmutableList();
}