﻿using Newtonsoft.Json;

namespace SWSG.BffService.Data.WebServices.Contracts;

public record EgidAttributesDto
{
    [JsonProperty("detail")] 
    public string Detail { get; init; } = string.Empty;

    [JsonProperty("featureId")]
    public string FeatureId { get; init; } = string.Empty;

    [JsonProperty("geom_quadindex")]
    public string GeomQuadIndex { get; init; } = string.Empty;

    [JsonProperty("geom_st_box2d")]
    public string GeomStBox2D { get; init; } = string.Empty;

    [JsonProperty("label")]
    public string Label { get; init; }= string.Empty;

    [JsonProperty("lat")]
    public double Lat { get; init; }

    [JsonProperty("lon")]
    public double Lon { get; init; }

    [JsonProperty("num")]
    public int Num { get; init; }

    [JsonProperty("objectclass")]
    public string ObjectClass { get; init; }

    [JsonProperty("origin")]
    public string Origin { get; init; } = string.Empty;
    
    [JsonProperty("rank")]
    public int Rank { get; init; }

    [JsonProperty("x")]
    public double X { get; init; }

    [JsonProperty("y")]
    public double Y { get; init; }

    [JsonProperty("zoomlevel")]
    public int ZoomLevel { get; init; }
}