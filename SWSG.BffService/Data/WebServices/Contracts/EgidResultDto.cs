﻿using Newtonsoft.Json;

namespace SWSG.BffService.Data.WebServices.Contracts;

public record EgidResultDto
{
    [JsonProperty("results")]
    public List<EgidDto> Egids { get; init; } = new();
};