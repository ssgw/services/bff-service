﻿using Newtonsoft.Json;

namespace SWSG.BffService.Data.WebServices.Contracts
{
    public record EgidDto
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("weight")]
        public int Weight { get; set; }

        [JsonProperty("attrs")] 
        public EgidAttributesDto Attributes { get; set; } = new();
    }
}