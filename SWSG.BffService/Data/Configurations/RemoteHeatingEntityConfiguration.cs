﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SWSG.BffService.Constants;
using SWSG.BffService.Data.Contracts;

namespace SWSG.BffService.Data.Configurations;

public sealed class RemoteHeatingEntityConfiguration : IEntityTypeConfiguration<RemoteHeatingEntity>
{
    public void Configure(EntityTypeBuilder<RemoteHeatingEntity> builder)
    {
        builder.ToTable(Db.EnergyDataManagement.Table.REMOTE_HEATING, Db.EnergyDataManagement.SCHEMA);

        builder.HasKey(rm => rm.Id);
        builder.Property(rm => rm.Id).IsRequired().HasColumnName("id").ValueGeneratedOnAdd();

        builder.Property(rm => rm.Name).HasColumnName("name").IsRequired(false);
        builder.Property(rm => rm.EnersourceMain).HasColumnName("enersource_main").IsRequired(false);
        builder.Property(rm => rm.Zip).HasColumnName("zip").IsRequired(false);
        builder.Property(rm => rm.Place).HasColumnName("place").IsRequired(false);
        builder.Property(rm => rm.BeginningOfOperation).HasColumnName("beginningofoperation").IsRequired(false);
        builder.Property(rm => rm.Energy).HasColumnName("energy").IsRequired(false);
        builder.Property(rm => rm.HouseConnections).HasColumnName("houseconnections").IsRequired(false);
        builder.Property(rm => rm.NetLength).HasColumnName("netlength").IsRequired(false);
        builder.Property(rm => rm.EnergySource).HasColumnName("energysource").IsRequired(false);
        builder.Property(rm => rm.PositionAccuracy).HasColumnName("positionaccuracy").IsRequired(false);
        builder.Property(rm => rm.X).HasColumnName("x").IsRequired(false);
        builder.Property(rm => rm.Y).HasColumnName("y").IsRequired(false);
        builder.Property(rm => rm.Operator).HasColumnName("operator").IsRequired(false);
        builder.Property(rm => rm.OperatorAddress).HasColumnName("operatoraddress").IsRequired(false);
        builder.Property(rm => rm.Phone).HasColumnName("phone").IsRequired(false);
    }
}