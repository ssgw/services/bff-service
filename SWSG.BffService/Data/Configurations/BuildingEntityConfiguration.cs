﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using SWSG.BffService.Constants;
using SWSG.BffService.Data.Contracts;

namespace SWSG.BffService.Data.Configurations;

public sealed class BuildingEntityConfiguration : IEntityTypeConfiguration<BuildingEntity>
{
    public void Configure(EntityTypeBuilder<BuildingEntity> builder)
    {
        builder.ToTable(Db.EnergyDataManagement.Table.BUILDINGS, Db.EnergyDataManagement.SCHEMA);

        builder.HasKey(building => building.Id);
        builder.Property(building => building.Id).IsRequired().HasColumnName("id").ValueGeneratedOnAdd();

        builder.Property(building => building.Egid).HasColumnName("egid").IsRequired(false);
        builder.Property(building => building.Longitude).HasColumnName("longitude").IsRequired(false);
        builder.Property(building => building.Latitude).HasColumnName("latitude").IsRequired(false);
        builder.Property(building => building.Address).HasColumnName("address").IsRequired(false);
        builder.Property(building => building.UmsetzungVon).HasColumnName("umsetzung_von").IsRequired(false);
        builder.Property(building => building.UmsetzungBis).HasColumnName("umsetzung_bis").IsRequired(false);
        builder.Property(building => building.Gstat).HasColumnName("gstat").IsRequired(false);
        builder.Property(building => building.Gkat).HasColumnName("gkat").IsRequired(false);
        builder.Property(building => building.Gastw).HasColumnName("gastw").IsRequired(false);
        builder.Property(building => building.Gklas).HasColumnName("gklas").IsRequired(false);
        builder.Property(building => building.Gbauj).HasColumnName("gbauj").IsRequired(false);
        builder.Property(building => building.Gbaum).HasColumnName("gbaum").IsRequired(false);
        builder.Property(building => building.Gebf).HasColumnName("gebf").IsRequired(false);
        builder.Property(building => building.Gwaerzh1).HasColumnName("gwaerzh1").IsRequired(false);
        builder.Property(building => building.Genh1).HasColumnName("genh1").IsRequired(false);
        builder.Property(building => building.Gwaerzh2).HasColumnName("gwaerzh2").IsRequired(false);
        builder.Property(building => building.Genh2).HasColumnName("genh2").IsRequired(false);
        builder.Property(building => building.Gwaerzw1).HasColumnName("gwaerzw1").IsRequired(false);
        builder.Property(building => building.Genw1).HasColumnName("genw1").IsRequired(false);
        builder.Property(building => building.Gwaerzw2).HasColumnName("gwaerzw2").IsRequired(false);
        builder.Property(building => building.Genw2).HasColumnName("genw2").IsRequired(false);
        builder.Property(building => building.HestiaBeschreibung).HasColumnName("hestia_beschreibung").IsRequired();
        builder.Property(building => building.Gasversorgung).HasColumnName("gasversorgung").IsRequired(false);
        builder.Property(building => building.WaermeVersorgung).HasColumnName("waermeversorgung").IsRequired(false);
        builder.Property(building => building.X).HasColumnName("x").IsRequired(false);
        builder.Property(building => building.Y).HasColumnName("y").IsRequired(false);
        builder.Property(building => building.DrillPossibilityState).HasColumnName("drill_possibility_state").IsRequired(false);
    }
}