﻿using Microsoft.EntityFrameworkCore;
using SWSG.BffService.DataAccess;

namespace SWSG.BffService.Data.Repositories;

public sealed class DatabaseProbeRepository : IDatabaseProbeRepository
{
    private ServiceDbContext DbContext { get; }

    public DatabaseProbeRepository(ServiceDbContext dbContext)
    {
        DbContext = dbContext;
    }

    public async Task<bool> IsDatabaseAliveAsync()
    {
        int success = await DbContext.Database.ExecuteSqlRawAsync("Select 1");
        return await Task.FromResult(Convert.ToBoolean(success));
    }
}
