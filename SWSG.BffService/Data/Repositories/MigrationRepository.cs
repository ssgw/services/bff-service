﻿using Microsoft.EntityFrameworkCore;
using SWSG.BffService.DataAccess;

namespace SWSG.BffService.Data.Repositories;

public sealed class MigrationRepository : IMigrationRepository
{
    private ServiceDbContext Context { get; }

    public MigrationRepository(ServiceDbContext context)
    {
        Context = context;
    }

    public async Task<bool> HasNoPendingMigrationAsync()
        => !(await Context.Database.GetPendingMigrationsAsync()).Any();

    public Task MigrateAsync()
        => Context.Database.MigrateAsync();
}