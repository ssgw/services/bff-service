﻿using Microsoft.EntityFrameworkCore;
using SWSG.BffService.Data.Contracts;
using SWSG.BffService.Data.Mapping;
using SWSG.BffService.DataAccess;
using SWSG.BffService.Domain;

namespace SWSG.BffService.Data.Repositories;

public sealed class PropertyEnergyDataRepository : IPropertyEnergyDataRepository
{
    private ServiceDbContext Context { get; }

    public PropertyEnergyDataRepository(ServiceDbContext context)
    {
        Context = context;
    }

    public async Task<PropertyEnergyData?> GetByEgidAsync(string egid)
        => (await Context
                .Set<BuildingEntity>()
                .FirstOrDefaultAsync(building => EF.Functions.ILike(egid, building.Egid))
            )?.ToPropertyEnergyData();
}