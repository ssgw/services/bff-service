﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SWSG.BffService.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddAdditionalBuildingProperties : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "Dplz4",
                schema: "ernergy-data",
                table: "buildings",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Garea",
                schema: "ernergy-data",
                table: "buildings",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gdekt",
                schema: "ernergy-data",
                table: "buildings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Ggdename",
                schema: "ernergy-data",
                table: "buildings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gvol",
                schema: "ernergy-data",
                table: "buildings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gwaerdath1",
                schema: "ernergy-data",
                table: "buildings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Gwaerdatw1",
                schema: "ernergy-data",
                table: "buildings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Gwaersceh1",
                schema: "ernergy-data",
                table: "buildings",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<int>(
                name: "Gwaerscew1",
                schema: "ernergy-data",
                table: "buildings",
                type: "integer",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PlzPlz6",
                schema: "ernergy-data",
                table: "buildings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "StrnameDeinr",
                schema: "ernergy-data",
                table: "buildings",
                type: "text",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Wkche",
                schema: "ernergy-data",
                table: "buildings",
                type: "text",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Dplz4",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "Garea",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "Gdekt",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "Ggdename",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "Gvol",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "Gwaerdath1",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "Gwaerdatw1",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "Gwaersceh1",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "Gwaerscew1",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "PlzPlz6",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "StrnameDeinr",
                schema: "ernergy-data",
                table: "buildings");

            migrationBuilder.DropColumn(
                name: "Wkche",
                schema: "ernergy-data",
                table: "buildings");
        }
    }
}
