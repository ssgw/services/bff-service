﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace SWSG.BffService.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddBuildings : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "ernergy-data");

            migrationBuilder.CreateTable(
                name: "buildings",
                schema: "ernergy-data",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    egid = table.Column<string>(type: "text", nullable: true),
                    longitude = table.Column<double>(type: "double precision", nullable: true),
                    latitude = table.Column<double>(type: "double precision", nullable: true),
                    address = table.Column<string>(type: "text", nullable: true),
                    umsetzung_von = table.Column<string>(type: "text", nullable: true),
                    umsetzung_bis = table.Column<string>(type: "text", nullable: true),
                    gstat = table.Column<int>(type: "integer", nullable: true),
                    gkat = table.Column<int>(type: "integer", nullable: true),
                    gklas = table.Column<int>(type: "integer", nullable: true),
                    gbauj = table.Column<int>(type: "integer", nullable: true),
                    gbaum = table.Column<string>(type: "text", nullable: true),
                    gebf = table.Column<string>(type: "text", nullable: true),
                    gwaerzh1 = table.Column<int>(type: "integer", nullable: true),
                    genh1 = table.Column<int>(type: "integer", nullable: true),
                    gwaerzh2 = table.Column<string>(type: "text", nullable: true),
                    genh2 = table.Column<string>(type: "text", nullable: true),
                    gwaerzw1 = table.Column<int>(type: "integer", nullable: true),
                    genw1 = table.Column<int>(type: "integer", nullable: true),
                    gwaerzw2 = table.Column<int>(type: "integer", nullable: true),
                    genw2 = table.Column<int>(type: "integer", nullable: true),
                    hestia_beschreibung = table.Column<string>(type: "text", nullable: false),
                    gasversorgung = table.Column<string>(type: "text", nullable: true),
                    waermeversorgung = table.Column<string>(type: "text", nullable: true),
                    x = table.Column<float>(type: "real", nullable: true),
                    y = table.Column<float>(type: "real", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_buildings", x => x.id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "buildings",
                schema: "ernergy-data");
        }
    }
}
