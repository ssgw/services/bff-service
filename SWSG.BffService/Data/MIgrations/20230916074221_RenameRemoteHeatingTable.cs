﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SWSG.BffService.Data.Migrations
{
    /// <inheritdoc />
    public partial class RenameRemoteHeatingTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_remote-heatings",
                schema: "ernergy-data",
                table: "remote-heatings");

            migrationBuilder.RenameTable(
                name: "remote-heatings",
                schema: "ernergy-data",
                newName: "remote_heatings",
                newSchema: "ernergy-data");

            migrationBuilder.AddPrimaryKey(
                name: "PK_remote_heatings",
                schema: "ernergy-data",
                table: "remote_heatings",
                column: "id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_remote_heatings",
                schema: "ernergy-data",
                table: "remote_heatings");

            migrationBuilder.RenameTable(
                name: "remote_heatings",
                schema: "ernergy-data",
                newName: "remote-heatings",
                newSchema: "ernergy-data");

            migrationBuilder.AddPrimaryKey(
                name: "PK_remote-heatings",
                schema: "ernergy-data",
                table: "remote-heatings",
                column: "id");
        }
    }
}
