﻿using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace SWSG.BffService.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddRemoteHeatingTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "remote-heatings",
                schema: "ernergy-data",
                columns: table => new
                {
                    id = table.Column<long>(type: "bigint", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    name = table.Column<string>(type: "text", nullable: true),
                    enersource_main = table.Column<string>(type: "text", nullable: true),
                    zip = table.Column<int>(type: "integer", nullable: true),
                    place = table.Column<string>(type: "text", nullable: true),
                    beginningofoperation = table.Column<int>(type: "integer", nullable: true),
                    energy = table.Column<int>(type: "integer", nullable: true),
                    houseconnections = table.Column<int>(type: "integer", nullable: true),
                    netlength = table.Column<int>(type: "integer", nullable: true),
                    energysource = table.Column<string>(type: "text", nullable: true),
                    positionaccuracy = table.Column<string>(type: "text", nullable: true),
                    x = table.Column<int>(type: "integer", nullable: true),
                    y = table.Column<int>(type: "integer", nullable: true),
                    @operator = table.Column<string>(name: "operator", type: "text", nullable: true),
                    operatoraddress = table.Column<string>(type: "text", nullable: true),
                    phone = table.Column<string>(type: "text", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_remote-heatings", x => x.id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "remote-heatings",
                schema: "ernergy-data");
        }
    }
}
