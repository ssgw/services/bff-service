﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SWSG.BffService.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddGastwToBuildingTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "gastw",
                schema: "ernergy-data",
                table: "buildings",
                type: "integer",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "gastw",
                schema: "ernergy-data",
                table: "buildings");
        }
    }
}
