﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace SWSG.BffService.Data.Migrations
{
    /// <inheritdoc />
    public partial class AddDrillPossibilityStateToBuilding : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "drill_possibility_state",
                schema: "ernergy-data",
                table: "buildings",
                type: "integer",
                nullable: true);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "drill_possibility_state",
                schema: "ernergy-data",
                table: "buildings");
        }
    }
}
