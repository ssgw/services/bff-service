﻿namespace SWSG.BffService.Data.Contracts;

public record RemoteHeatingEntity
{
    public long Id { get; init; }
    public string? Name { get; init; }
    public string? EnersourceMain { get; init; }
    public int? Zip { get; init; }
    public string? Place { get; init; }
    public int? BeginningOfOperation { get; init; }
    public int? Energy { get; init; }
    public int? HouseConnections { get; init; }
    public int? NetLength { get; init; }
    public string? EnergySource { get; init; }
    public string? PositionAccuracy { get; init; }
    public int? X { get; init; }
    public int? Y { get; init; }
    public string? Operator { get; init; }
    public string? OperatorAddress { get; init; }
    public string? Phone { get; init; }
}