﻿namespace SWSG.BffService.Data.Contracts;

public record BuildingEntity
{
    public long Id { get; init; }
    public string? Egid { get; init; }
    public double? Longitude { get; init; }
    public double? Latitude { get; init; }
    public string? Address { get; init; }
    public string? UmsetzungVon { get; init; }
    public string? UmsetzungBis { get; init; }
    public string? StrnameDeinr { get; init; }
    public string? PlzPlz6 { get; init; }
    public string? Ggdename { get; init; }
    // public int? Ggdenr { get; init; }
    // public string? Gexpdat { get; init; }
    public string? Gdekt { get; init; }
    // public string? Egrid { get; init; }
    // public int? Lgbkr { get; init; }
    // public int? Lparz { get; init; }
    // public string? Lparzsx { get; init; }
    // public string? Ltyp { get; init; }
    // public string? Gebnr { get; init; }
    // public string? Gbez { get; init; }
    // public double? Gkode { get; init; }
    // public double? Gkodn { get; init; }
    // public int? Gksce { get; init; }
    public int? Gstat { get; init; }
    public int? Gkat { get; init; }
    public int? Gklas { get; init; }
    public int? Gbauj { get; init; }
    public string? Gbaum { get; init; }
    // public int? Gbaup { get; init; }
    // public string? Gabbj { get; init; }
    public int? Garea { get; init; }
    public string? Gvol { get; init; }
    // public string? Gvolnorm { get; init; }
    // public string? Gvolsce { get; init; }
    public int? Gastw { get; init; }
    // public int? Ganzwhg { get; init; }
    // public string? Gazzi { get; init; }
    // public string? Gschutzr { get; init; }
    public string? Gebf { get; init; }
    public int? Gwaerzh1 { get; init; }
    public int? Genh1 { get; init; }
    public int? Gwaersceh1 { get; init; }
    public string? Gwaerdath1 { get; init; }
    public string? Gwaerzh2 { get; init; }
    public string? Genh2 { get; init; }
    // public string? Gwaersceh2 { get; init; }
    // public string? Gwaerdath2 { get; init; }
    public int? Gwaerzw1 { get; init; }
    public int? Genw1 { get; init; }
    public int? Gwaerscew1 { get; init; }
    public string? Gwaerdatw1 { get; init; }
    public int? Gwaerzw2 { get; init; }
    public int? Genw2 { get; init; }
    // public int? Gwaerscew2 { get; init; }
    // public string? Gwaerdatw2 { get; init; }
    // public string? Edid { get; init; }
    // public int? Egaid { get; init; }
    // public string? Strname { get; init; }
    // public string? Strnamk { get; init; }
    // public string? Strindx { get; init; }
    // public string? Strsp { get; init; }
    // public string? Stroffiziel { get; init; }
    public int? Dplz4 { get; init; }
    // public int? Dplzz { get; init; }
    // public float? Dkode { get; init; }
    // public float? Dkodn { get; init; }
    // public int? Doffadr { get; init; }
    // public string? Dexpdat { get; init; }
    // public string? Ewid { get; init; }
    // public string? Whgnr { get; init; }
    // public string? Wstwk { get; init; }
    // public string? Wmehrg { get; init; }
    // public string? Weinr { get; init; }
    // public string? Wbez { get; init; }
    // public string? Wstat { get; init; }
    // public string? Wexpdat { get; init; }
    // public string? Wbauj { get; init; }
    // public string? Wabbj { get; init; }
    // public string? Warea { get; init; }
    // public string? Wazim { get; init; }
    public string? Wkche { get; init; }
    public string HestiaBeschreibung { get; init; }
    public string? Gasversorgung { get; init; }
    public string? WaermeVersorgung { get; init; }
    public float? X { get; init; }
    public float? Y { get; init; }
    public int? DrillPossibilityState { get; init; }
}