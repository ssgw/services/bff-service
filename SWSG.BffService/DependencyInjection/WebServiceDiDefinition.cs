﻿using SWSG.BffService.Data.WebServices;
using SWSG.BffService.DataAccess;

namespace SWSG.BffService.DependencyInjection;

public static class WebServiceDiDefinition
{
    public static IServiceCollection AddWebServiceDiDefinition(this IServiceCollection services)
        => services
            .AddScoped<IEgidWebService, EgidWebService>();
}