﻿using SWSG.BffService.Core;
using SWSG.BffService.CoreAccess;

namespace SWSG.BffService.DependencyInjection;

public static class ServiceDiDefinition
{
    public static IServiceCollection AddServiceDiDefinition(this IServiceCollection services)
        => services
            .AddScoped<IMonitoringService, MonitoringService>()
            .AddScoped<IPersistenceService, PersistenceService>()
            .AddScoped<IPropertyDataService, PropertyDataService>();
}