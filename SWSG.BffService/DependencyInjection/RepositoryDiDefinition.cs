﻿using SWSG.BffService.Data.Repositories;
using SWSG.BffService.DataAccess;

namespace SWSG.BffService.DependencyInjection;

public static class RepositoryDiDefinition
{
    public static IServiceCollection AddRepositoryDiDefinition(this IServiceCollection services)
        => services
            .AddTransient<IDatabaseProbeRepository, DatabaseProbeRepository>()
            .AddTransient<IMigrationRepository, MigrationRepository>()
            .AddTransient<IPropertyEnergyDataRepository, PropertyEnergyDataRepository>();
}