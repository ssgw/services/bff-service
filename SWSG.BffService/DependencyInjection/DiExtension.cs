﻿namespace SWSG.BffService.DependencyInjection;

public static class DiExtension
{
    public static IServiceCollection AddDiDefinitions(this IServiceCollection services)
        => services
            .AddMonitorDiDefinition()
            .AddRepositoryDiDefinition()
            .AddServiceDiDefinition()
            .AddWebServiceDiDefinition();
}