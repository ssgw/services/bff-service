﻿using SWSG.BffService.Core.Monitoring;

namespace SWSG.BffService.DependencyInjection;

public static class MonitorDiDefinition
{
    public static IServiceCollection AddMonitorDiDefinition(this IServiceCollection services)
        => services
            .AddTransient<IDatabaseMonitor, DatabaseMonitor>();
}