using SWSG.BffService.Api.Configurations;
using SWSG.BffService.Api.Monitoring;
using SWSG.BffService.Api.Monitoring.Extensions;
using SWSG.BffService.DependencyInjection;

WebApplicationBuilder builder = WebApplication.CreateBuilder(args);
builder.Services.AddControllers();
builder.Services.AddApiVersioning();
builder.Services.AddSwagger("BFF-Service");
builder.Services.AddDiDefinitions();
builder.Services.ConfigureService(builder.Configuration);
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddHealthChecks<LivenessCheck, ReadinessCheck>();
builder.Services.AddAllowAllCors();

WebApplication app = builder.Build();
app.UsePathBase("/bff-service");
app.UseRouting();
app.UseAllowAllCors();
app.UseSwagger("/bff-service");
app.MapControllers();
app.MapHealthChecks();

await app.ApplyDataStructureAsync();

app.Run();